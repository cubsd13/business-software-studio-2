﻿set character_set_client='utf8';
set character_set_connection='utf8';
set character_set_database='utf8';
set character_set_results='utf8';
set character_set_server='utf8';

CREATE DATABASE IF NOT EXISTS `bss` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `bss`;

CREATE TABLE `box` (
  `boxId` int(10) NOT NULL COMMENT 'รหัสกล่อง',
  `name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อกล่อง',
  `width` double NOT NULL COMMENT 'ความกว้างกล่อง',
  `length` double NOT NULL COMMENT 'ความยาวกล่อง',
  `height` double NOT NULL COMMENT 'ความสูงกล่อง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลกล่อง';

CREATE TABLE `category` (
  `categoryId` int(10) NOT NULL COMMENT 'รหัสหมวดหมู่สินค้า',
  `categoryname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อหมวดหมู่สินค้า'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลหมวดหมู่สินค้า';

CREATE TABLE `layer` (
  `layerId` int(10) NOT NULL COMMENT 'รหัสเลเยอร์',
  `resultboxId` int(10) NOT NULL COMMENT 'รหัสผลลัพธ์การจัดกล่อง',
  `width` double NOT NULL COMMENT 'ความกว้างเลเยอร์',
  `length` double NOT NULL COMMENT 'ความยาวเลเยอร์',
  `seqNumber` int(10) NOT NULL COMMENT 'ลําดับของเลเยอร์ในกล่อง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลเลเยอร์' ROW_FORMAT=COMPACT;

CREATE TABLE `layerproduct` (
  `layerproductId` int(10) NOT NULL COMMENT 'รหัสเลเยอร์สินค้า',
  `productId` int(10)  NOT NULL COMMENT 'รหัสสินค้า',
  `layerId` int(10) NOT NULL COMMENT 'รหัสเลเยอร์',
  `x` int(20) NOT NULL COMMENT 'ตําแหน่ง x ของสินค้าที่อยู่ในเลเยอร',
  `y` int(20) NOT NULL COMMENT 'ตําแหน่ง y ของสินค้าที่อยู่ในเลเยอร์'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลเลเยอร์ สินค้า';

CREATE TABLE `logisticbox` (
  `logisticboxId` int(10) NOT NULL COMMENT 'รหัสข้อมูลกล่องของขนส่ง',
  `logisticId` int(10) NOT NULL COMMENT 'รหัสบริษัทขนส่ง',
  `boxId` int(10) NOT NULL COMMENT 'รหัสกล่อง',
  `price` double NOT NULL COMMENT 'ราคาค่าขนส่ง',
  `weight` double NOT NULL COMMENT 'นํ้าหนักสูงสุด'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลกล่องของบริษัทขนส่ง';

CREATE TABLE `logisticinfo` (
  `logisticId` int(10) NOT NULL COMMENT 'รหัสบริษัทขนส่ง',
  `name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อบริษัทขนส่ง',
  `telephone` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เบอร์ติดต่อขนส่ง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลบริษัทขนส่ง';

CREATE TABLE `orders` (
  `ordersId` int(10) NOT NULL COMMENT 'รหัสคําสั่งขาย',
  `ordersNumber` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'เลขที่คําสั่งขาย',
  `customerName` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อลูกค้า',
  `customerAddress` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ที่อยู่จัดส่ง',
  `date` date NOT NULL COMMENT 'วันที่คําสั่งขาย',
  `status` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'สถานะคำสั่งขาย'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลคําสั่งขาย';

CREATE TABLE `ordersdetail` (
  `ordersdetailId` int(10) NOT NULL COMMENT 'รหัสรายละเอียดคําสั่งขาย',
  `productId` int(10)  NOT NULL COMMENT 'รหัสสินค้า',
  `ordersId` int(10) NOT NULL COMMENT 'รหัสคําสั่งขาย',
  `qty` int(11) NOT NULL COMMENT 'จํานวนสินค้า'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลรายละเอียดคำสั่งซื้อ';

CREATE TABLE `product` (
  `productId` int(10) NOT NULL COMMENT 'รหัสสินค้า',
  `categoryId` int(10) NOT NULL COMMENT 'รหัสหมวดหมู่สินค้า',
  `name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อสินค้า',
  `width` double NOT NULL COMMENT 'ความกว้างสินค้า',
  `length` double NOT NULL COMMENT 'ความยาวสินค้า',
  `height` double NOT NULL COMMENT 'ความสูงสินค้า',
  `weight` double NOT NULL COMMENT 'นํ้าหนักสินค้า',
  `frontView` tinyint(1) NOT NULL COMMENT 'วางด้าน Front View ลงกล่องได้ หรือไม่',
  `topView` tinyint(1) NOT NULL COMMENT 'วางด้าน Top View ลงกล่องได้ หรือไม่',
  `sideView` tinyint(1) NOT NULL COMMENT 'วางด้าน Side View ลงกล่องได้ หรือไม่',
  `isArranged` TINYINT(1) NOT NULL COMMENT 'ถูกนำไปคำนวณการจัดเรียง หรือไม่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลสินค้า';

CREATE TABLE `resultbox` (
  `resultboxId` int(10) NOT NULL COMMENT 'รหัสผลลัพธ์การจัดกล่อง',
  `resultorderingId` int(10) NOT NULL COMMENT 'รหัสรายการผลลัพธ์ของคําสั่งขาย',
  `logisticboxId` int(10) NOT NULL COMMENT 'รหัสข้อมูลกล่องของขนส่ง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลผลลัพธ์การจัดกล่อง';

CREATE TABLE `resultordering` (
  `resultorderingId` int(10) NOT NULL COMMENT 'รหัสรายการผลลัพธ์ของคําสั่งขาย',
  `batchId` int(10) NOT NULL COMMENT 'รหัสแบช',
  `ordersId` int(10) NOT NULL COMMENT 'รหัสคําสั่งขาย'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลรายการผลลัพธ์ของคําสั่งขาย';

CREATE TABLE `user` (
  `userId` int(10) NOT NULL COMMENT 'รหัสผู้ใช้งาน',
  `username` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อผู้ใช้งาน',
  `password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'รหัสผ่านเข้าใช้งาน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลผู้ใช้งาน';

CREATE TABLE `batch` (
  `batchId` int(11) NOT NULL COMMENT 'รหัสแบช',
  `datetime` datetime NOT NULL COMMENT 'เวลาตัดรอบ',
  `logisticId` int(11) NOT NULL COMMENT 'รหัสบริษัทขนส่ง',
  `sequence` int(11) NOT NULL COMMENT 'ลำดับ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT 'ข้อมูลแบช';

CREATE TABLE `master`(
`id` INT(10) NOT NULL COMMENT 'รหัสข้อมูล',
`key` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'โค้ดข้อมูล',
`value` text COLLATE UTF8_UNICODE_CI NOT NULL COMMENT 'ค่าข้อมูล'
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='ข้อมูลมาสเตอร์';
