set character_set_client='utf8';
set character_set_connection='utf8';
set character_set_database='utf8';
set character_set_results='utf8';
set character_set_server='utf8';

USE `bss`;

ALTER TABLE `box`
  ADD PRIMARY KEY (`boxId`);

ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

ALTER TABLE `layer`
  ADD PRIMARY KEY (`layerId`),
  ADD KEY `fk_layer_resultboxId` (`resultboxId`);

ALTER TABLE `layerproduct`
  ADD PRIMARY KEY (`layerproductId`),
  ADD UNIQUE KEY `PRIMARY2` (`productId`),
  ADD KEY `fk_layerproduct_layertId` (`layerId`);

ALTER TABLE `logisticbox`
  ADD PRIMARY KEY (`logisticboxId`),
  ADD KEY `fk_logisticbox_logisticId` (`logisticId`),
  ADD KEY `fk_logisticbox_boxId` (`boxId`);

ALTER TABLE `logisticinfo`
  ADD PRIMARY KEY (`logisticId`);

ALTER TABLE `orders`
  ADD PRIMARY KEY (`ordersId`);

ALTER TABLE `ordersdetail`
  ADD PRIMARY KEY (`ordersdetailId`),
  ADD KEY `fk_ordersdetail_ordersId` (`ordersId`),
  ADD KEY `fk_ordersdetail_productid` (`productId`);

ALTER TABLE `product`
  ADD PRIMARY KEY (`productId`),
  ADD KEY `fk_product_categoryId` (`categoryId`);

ALTER TABLE `resultbox`
  ADD PRIMARY KEY (`resultboxId`),
  ADD KEY `fk_resultbox_resultorderingId` (`resultorderingId`),
  ADD KEY `fk_resultbox_logisticboxId` (`logisticboxId`);

ALTER TABLE `resultordering`
  ADD PRIMARY KEY (`resultorderingId`),
  ADD KEY `fk_resultordering_ordersId` (`ordersId`),
  ADD KEY `fk_resultordering_batchId` (`batchId`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

ALTER TABLE `batch`
  ADD PRIMARY KEY (`batchId`),
  ADD KEY `fk_batch_logisticId` (`logisticId`);

ALTER TABLE `master`
ADD PRIMARY KEY (`id`);