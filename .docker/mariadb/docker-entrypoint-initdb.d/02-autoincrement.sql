﻿set character_set_client='utf8';
set character_set_connection='utf8';
set character_set_database='utf8';
set character_set_results='utf8';
set character_set_server='utf8';

USE `bss`;

ALTER TABLE `box`
  MODIFY `boxId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสกล่อง';

ALTER TABLE `category`
  MODIFY `categoryId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสหมวดหมู่สินค้า';

ALTER TABLE `layer`
  MODIFY `layerId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสเลเยอร์';

ALTER TABLE `layerproduct`
  MODIFY `layerproductId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสเลเยอร์สินค้า';

ALTER TABLE `logisticbox`
  MODIFY `logisticboxId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสข้อมูลกล่องของขนส่ง';

ALTER TABLE `logisticinfo`
  MODIFY `logisticId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสบริษัทขนส่ง';

ALTER TABLE `orders`
  MODIFY `ordersId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสคําสั่งขาย';

ALTER TABLE `ordersdetail`
  MODIFY `ordersdetailId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายละเอียดคําสั่งขาย';

ALTER TABLE `resultbox`
  MODIFY `resultboxId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสผลลัพธ์การจัดกล่อง';

ALTER TABLE `resultordering`
  MODIFY `resultorderingId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสรายการผลลัพธ์ของคําสั่งขาย';

ALTER TABLE `user`
  MODIFY `userId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสผู้ใช้งาน';

ALTER TABLE `product`
  MODIFY `productId` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสสินค้า';

ALTER TABLE `batch`
  MODIFY `batchId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแบช';

ALTER TABLE `master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสข้อมูล';
  
