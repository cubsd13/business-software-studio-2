using SBA.DataModels;
using SBA.Repositories;

namespace SBA.Logic
{
    public class CalculateBox
    {
        private readonly BoxRepository _boxRepo;

        public CalculateBox(bssContext context)
        {
            _boxRepo = new BoxRepository(context);
        }

        public void GetBox()
        {
            var boxlist = _boxRepo.GetAllBoxes();
        }
    }
}