using SBA.DataModels;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class ProductRepository
    {
        private readonly bssContext _db;

        public ProductRepository(bssContext context)
        {
            _db = context;
        }

        public List<Product> GetAllProduct()
        {
            return _db.Product
                    .Join(_db.Category,
                          p => p.CategoryId,
                          c => c.CategoryId,
                          (p, c) => new Product
                          {
                              ProductId = p.ProductId,
                              CategoryId = p.CategoryId,
                              Categoryname = c.Categoryname,
                              Name = p.Name,
                              Width = p.Width,
                              Height = p.Height,
                              Length = p.Length,
                              Weight = p.Weight,
                              FrontView = p.FrontView,
                              SideView = p.SideView,
                              TopView = p.TopView,
                              IsArranged = p.IsArranged
                          }).OrderBy(p => p.ProductId).ToList();
        }

        public bool AddProduct(Product addedPrd)
        {
            try
            {
                _db.Product.Add(addedPrd);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateProduct(Product modifiedPrd)
        {
            try
            {
                var productToUpdate = _db.Product.Find(modifiedPrd.ProductId);
                productToUpdate.Name = modifiedPrd.Name;
                productToUpdate.Length = modifiedPrd.Length;
                productToUpdate.Width = modifiedPrd.Width;
                productToUpdate.Height = modifiedPrd.Height;
                productToUpdate.Weight = modifiedPrd.Weight;
                productToUpdate.TopView = modifiedPrd.TopView;
                productToUpdate.SideView = modifiedPrd.SideView;
                productToUpdate.FrontView = modifiedPrd.FrontView;
                productToUpdate.IsArranged = modifiedPrd.IsArranged;
                productToUpdate.CategoryId = modifiedPrd.CategoryId;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal List<Product> GetProductIsArranged()
        {
            return _db.Product
                .Where(p => p.IsArranged == true)
                .Join(_db.Category,
                      p => p.CategoryId,
                      c => c.CategoryId,
                      (p, c) => new Product
                      {
                          ProductId = p.ProductId,
                          CategoryId = p.CategoryId,
                          Categoryname = c.Categoryname,
                          Name = p.Name,
                          Width = p.Width,
                          Height = p.Height,
                          Length = p.Length,
                          Weight = p.Weight,
                          FrontView = p.FrontView,
                          SideView = p.SideView,
                          TopView = p.TopView,
                          IsArranged = p.IsArranged
                      }).OrderBy(p => p.ProductId).ToList();
        }

        internal List<Product> GetProductNotArranged()
        {
            return _db.Product
               .Where(p => p.IsArranged == false)
               .Join(_db.Category,
                     p => p.CategoryId,
                     c => c.CategoryId,
                     (p, c) => new Product
                     {
                         ProductId = p.ProductId,
                         CategoryId = p.CategoryId,
                         Categoryname = c.Categoryname,
                         Name = p.Name,
                         Width = p.Width,
                         Height = p.Height,
                         Length = p.Length,
                         Weight = p.Weight,
                         FrontView = p.FrontView,
                         SideView = p.SideView,
                         TopView = p.TopView,
                         IsArranged = p.IsArranged
                     }).OrderBy(p => p.ProductId).ToList();
        }
    }
}