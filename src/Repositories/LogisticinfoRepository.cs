using SBA.DataModels;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class LogisticinfoRepository
    {
        private readonly bssContext context;

        public LogisticinfoRepository(bssContext context)
        {
            this.context = context;
        }

        public List<Logisticinfo> GetAllLogisticinfo()
        {
            var data = context.Logisticinfo.ToList();
            data.ForEach(p => p.boxAmount = context.Logisticbox.Where(l => l.LogisticId == p.LogisticId).Count());
            return data;
        }

        public List<Logisticbox> GetLogisticboxById(int id)
        {
            return context.Logisticbox
                    .Join(context.Box,
                            lb => lb.BoxId,
                            b => b.BoxId,
                            (lb, b) => new
                            {
                                LogisticId = lb.LogisticId,
                                LogisticboxId = lb.LogisticboxId,
                                BoxId = lb.BoxId,
                                BoxName = b.Name,
                                Price = lb.Price,
                                Weight = lb.Weight
                            })
                    .Where(p => p.LogisticId == id)
                    .Select(p => new Logisticbox
                    {
                        LogisticboxId = p.LogisticboxId,
                        LogisticId = p.LogisticId,
                        BoxId = p.BoxId,
                        BoxName = p.BoxName,
                        Price = p.Price,
                        Weight = p.Weight
                    }).ToList();
        }

        public bool AddLogisticInfo(Logisticinfo obj)
        {
            try
            {
                context.Logisticinfo.Add(obj);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateLogisticInfo(Logisticinfo obj)
        {
            try
            {
                var data = context.Logisticinfo.Where(p => p.LogisticId == obj.LogisticId).FirstOrDefault();
                if (data == null) return false;
                data.Name = obj.Name;
                data.Telephone = obj.Telephone;
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Logisticinfo GetLogisticInfobyId(int id)
        {
            return context.Logisticinfo.Where(l => l.LogisticId == id).FirstOrDefault();
        }
    }
}