using SBA.DataModels;
using src.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class UserRepository
    {
        private readonly bssContext _db;

        public UserRepository(bssContext context)
        {
            _db = context;
        }

        public List<User> GetAllUsers()
        {
            List<User> result = _db.User.ToList();
            return result;
        }

        public User Login(UserLoginModel requestModel)
        {
            return _db.User.Where(x =>
              x.Username.ToLower().Equals(requestModel.Username.ToLower())
              && x.Password.ToLower().Equals(requestModel.Password.ToLower())
            ).FirstOrDefault();
        }

        public bool CreateUser(User User)
        {
            try
            {
                _db.User.Add(User);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateUser(User user)
        {
            try
            {
                var data = _db.User.Where(p => p.UserId == user.UserId).FirstOrDefault();
                if (data == null) return false;
                data.Password = user.Password;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}