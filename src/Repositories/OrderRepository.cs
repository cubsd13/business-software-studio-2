using SBA.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class OrderRepository
    {
        private readonly bssContext _db;

        public OrderRepository(bssContext context)
        {
            _db = context;
        }

        public List<Orders> GetAllOrders()
        {
            var data = _db.Orders.Where(p => p.Status == OrderStatus.NEW || p.Status == OrderStatus.EDIT).ToList();
            data.ForEach(p => p.TotalProductQty = _db.Ordersdetail.Where(d => d.OrdersId == p.OrdersId).Sum(a => a.Qty));
            return data;
        }

        public Orders GetOrderByOrderNumber(string ordersnumber)
        {
            var obj = _db.Orders.Where(p => p.OrdersNumber == ordersnumber && (p.Status == OrderStatus.NEW || p.Status == OrderStatus.EDIT)).FirstOrDefault();
            if (obj != null)
            {
                obj.OrdersDetails = _db.Ordersdetail
                                    .Join(_db.Product,
                                           o => o.ProductId,
                                           p => p.ProductId,
                                            (o, p) => new Ordersdetail
                                            {
                                                ProductId = o.ProductId,
                                                OrdersId = o.OrdersId,
                                                OrdersdetailId = o.OrdersdetailId,
                                                Qty = o.Qty,
                                                ProductName = p.Name
                                            })
                                      .Where(p => p.OrdersId == obj.OrdersId)
                                      .ToList();
                obj.TotalProductQty = obj.OrdersDetails.Select(p => p.Qty).Sum();
            }
            return obj;
        }
        public Orders GetOrderAllByOrderNumber(string ordersnumber)
        {
            var obj = _db.Orders.Where(p => p.OrdersNumber == ordersnumber).FirstOrDefault();
            if (obj != null)
            {
                obj.OrdersDetails = _db.Ordersdetail
                                    .Join(_db.Product,
                                           o => o.ProductId,
                                           p => p.ProductId,
                                            (o, p) => new Ordersdetail
                                            {
                                                ProductId = o.ProductId,
                                                OrdersId = o.OrdersId,
                                                OrdersdetailId = o.OrdersdetailId,
                                                Qty = o.Qty,
                                                ProductName = p.Name
                                            })
                                      .Where(p => p.OrdersId == obj.OrdersId)
                                      .ToList();
                obj.TotalProductQty = obj.OrdersDetails.Select(p => p.Qty).Sum();
            }
            return obj;
        }
        public bool EditOrderDetail(Ordersdetail obj)
        {
            try
            {
                var data = _db.Ordersdetail.Where(p => p.OrdersdetailId == obj.OrdersdetailId).FirstOrDefault();
                if (data == null) return false;
                data.ProductId = obj.ProductId;
                data.Qty = obj.Qty;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddOrderDetail(Ordersdetail obj)
        {
            try
            {
                _db.Ordersdetail.Add(obj);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddOrder(Orders obj)
        {
            try
            {
                obj.Status = OrderStatus.NEW;
                _db.Orders.Add(obj);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditOrder(Orders obj)
        {
            try
            {
                var data = _db.Orders.Where(p => p.OrdersId == obj.OrdersId).FirstOrDefault();
                if (data == null) return false;
                data.Status = OrderStatus.EDIT;
                data.OrdersNumber = obj.OrdersNumber;
                data.CustomerName = obj.CustomerName;
                data.CustomerAddress = obj.CustomerAddress;
                data.Date = obj.Date;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteOrder(int ordersid)
        {
            try
            {
                var details = _db.Ordersdetail.Where(p => p.OrdersId == ordersid).ToList();
                foreach (var i in details)
                {
                    _db.Ordersdetail.Remove(i);
                }
                var order = _db.Orders.Where(p => p.OrdersId == ordersid).FirstOrDefault();
                _db.Orders.Remove(order);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteOrderdetail(int ordersdetailid)
        {
            try
            {
                var detail = _db.Ordersdetail.Where(p => p.OrdersdetailId == ordersdetailid).FirstOrDefault();
                _db.Ordersdetail.Remove(detail);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Orders GetOrderById(int id)
        {
            var obj = _db.Orders.Where(p => p.OrdersId == id).FirstOrDefault();
            if (obj != null)
            {
                obj.OrdersDetails = _db.Ordersdetail
                                    .Join(_db.Product,
                                           o => o.ProductId,
                                           p => p.ProductId,
                                            (o, p) => new Ordersdetail
                                            {
                                                ProductId = o.ProductId,
                                                OrdersId = o.OrdersId,
                                                OrdersdetailId = o.OrdersdetailId,
                                                Qty = o.Qty,
                                                ProductName = p.Name
                                            })
                                      .Where(p => p.OrdersId == obj.OrdersId)
                                      .ToList();
                obj.TotalProductQty = obj.OrdersDetails.Select(p => p.Qty).Sum();
            }
            return obj;
        }

        public bool SetOrderStatusByOrdersNumber(String ordNum)
        {
            try
            {
                var data = _db.Orders.Where(p => p.OrdersNumber == ordNum).FirstOrDefault();
                if (data == null) return false;
                data.Status = OrderStatus.ARRANGED;
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool isArranged(List<int> orderIds)
        {
            var isArranged = false;
            try
            {
                foreach (var i in orderIds)
                {
                    String orderNumber = GetOrderById(i).OrdersNumber;
                    var order = _db.Orders.Where(p => p.OrdersNumber == orderNumber).FirstOrDefault();
                    if (order.Status == "ARRANGED")
                    {
                        isArranged = true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return isArranged;
            }
            catch
            {
                return false;
            }
        }
    }
}