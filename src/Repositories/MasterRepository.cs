using SBA.DataModels;
using System.Linq;

namespace SBA.Repositories
{
    public class MasterRepository
    {
        private readonly bssContext context;

        public MasterRepository(bssContext _context)
        {
            context = _context;
        }

        public ShippingAddressReport GetShippingAddress(int orderid)
        {
            var obj = context.Orders.Where(p => p.OrdersId == orderid).First();
            var result = GetCompanyInfo();
            result.SetCustomerName(obj.CustomerName);
            result.SetCustomerAddress(obj.CustomerAddress);
            return result;
        }

        public ShippingAddressReport GetCompanyInfo()
        {
            var keyArray = new string[] { "companyname", "companyaddress", "companyzipcode", "companyphone" };
            return new ShippingAddressReport()
            {
                CompanyName = context.Master.Where(p => p.Key.Equals(keyArray[0])).Select(p => p.Value).First(),
                CompanyAddress = context.Master.Where(p => p.Key.Equals(keyArray[1])).Select(p => p.Value).First(),
                CompanyZipcode = context.Master.Where(p => p.Key.Equals(keyArray[2])).Select(p => p.Value).First(),
                CompanyPhone = context.Master.Where(p => p.Key.Equals(keyArray[3])).Select(p => p.Value).First()
            };
        }
    }
}