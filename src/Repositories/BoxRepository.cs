using SBA.DataModels;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class BoxRepository
    {
        private readonly bssContext _db;

        public BoxRepository(bssContext context)
        {
            _db = context;
        }

        public List<Box> GetAllBoxes()
        {
            return _db.Box.ToList();
        }

        public bool UpdateBox(Logisticbox obj)
        {
            try
            {
                var data = _db.Logisticbox.Where(p => p.LogisticboxId == obj.LogisticboxId).FirstOrDefault();
                if (data == null) return false;
                data.BoxId = obj.BoxId;
                data.BoxName = obj.BoxName;
                data.Price = obj.Price;
                data.Weight = obj.Weight;

                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddBox(Logisticbox obj)
        {
            try
            {
                _db.Logisticbox.Add(obj);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}