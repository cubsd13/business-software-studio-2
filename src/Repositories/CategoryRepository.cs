using SBA.DataModels;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class CategoryRepository
    {
        private readonly bssContext _db;

        public CategoryRepository(bssContext context)
        {
            _db = context;
        }

        public List<Category> GetAllCategory()
        {
            return _db.Category.ToList();
        }

        public bool AddCategory(Category addedCtg)
        {
            _db.Category.Add(addedCtg);
            _db.SaveChanges();
            return true;
        }

        public bool UpdateCategory(Category modifiedCtg)
        {
            var categoryToUpdate = _db.Category.Find(modifiedCtg.CategoryId);
            categoryToUpdate.Categoryname = modifiedCtg.Categoryname;

            _db.SaveChanges();
            return true;
        }
    }
}