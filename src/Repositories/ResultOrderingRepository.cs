using SBA.DataModels;
using System.Linq;

namespace SBA.Repositories
{
    public class ResultOrderingRepository
    {
        private readonly bssContext _db;

        public ResultOrderingRepository(bssContext context)
        {
            _db = context;
        }

        public bool AddResultOrdering(Resultordering resultordering)
        {
            try
            {
                _db.Resultordering.Add(resultordering);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int GetTotalOrderByBatchID(int BatchId)
        {
            int TotalOrder = _db.Resultordering.Where(t => t.BatchId == BatchId).Count();

            return TotalOrder;
        }
    }
}