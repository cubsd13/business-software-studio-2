using SBA.Algorithm.DataFeed;
using SBA.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Repositories
{
    public class BatchRepository
    {
        private readonly bssContext _db;

        public BatchRepository(bssContext context)
        {
            _db = context;
        }

        public List<Batch> GetAllBatchs()
        {
            List<Batch> result = new List<Batch>();
            var data = _db.Batch.OrderByDescending(x => x.Datetime).ToList();
            result = data;
            LogisticinfoRepository _logisticInfoRepo = new LogisticinfoRepository(new Context());
            foreach (var i in result)
            {
                var logname = _logisticInfoRepo.GetLogisticInfobyId(i.LogisticId);
                i.LogisticName = logname.Name;
                i.TotalOrder = this.GetTotalOrderByBatchID(i.BatchId);
            }
            return result;
        }

        public bool AddBatch(Batch batch)
        {
            try
            {
                _db.Batch.Add(batch);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int GetBatchIdSequence(DateTime datetime)
        {
            try
            {
                List<Batch> data = _db.Batch.Where(p => p.Datetime.Date == datetime.Date).ToList();
                int sequence = data.Count();
                return sequence + 1;
            }
            catch
            {
                return 1;
            }
        }

        public int GetLastBatchId()
        {
            try
            {
                Batch data = _db.Batch.OrderByDescending(p => p.BatchId).FirstOrDefault();
                return data.BatchId;
            }
            catch
            {
                return 1;
            }
        }

        public int GetTotalOrderByBatchID(int BatchId)
        {
            ResultOrderingRepository resultordering = new ResultOrderingRepository(new Context());
            return resultordering.GetTotalOrderByBatchID(BatchId);
        }

        public List<int> GetOrderListByBatchId(int BatchId)
        {
            List<Resultordering> resultOrdering = _db.Resultordering.Where(b => b.BatchId == BatchId).ToList();
            List<int> result = new List<int>();
            foreach (var i in resultOrdering)
            {
                result.Add(i.OrdersId);
            }
            return result;
        }
    }
}