using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;

namespace SBA
{
    public class Program
    {
        public static Int64 cycleCount = 0;

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(serverOptions =>
                    {
                        // serverOptions.Limits.RequestHeadersTimeout = TimeSpan.FromMinutes(5);
                    }).UseStartup<Startup>();
                });
    }
}