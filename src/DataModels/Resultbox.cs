﻿namespace SBA.DataModels
{
    public partial class Resultbox
    {
        public int ResultboxId { get; set; }
        public int ResultorderingId { get; set; }
        public int LogisticboxId { get; set; }
    }
}