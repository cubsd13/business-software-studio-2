﻿namespace SBA.DataModels
{
    public partial class Layerproduct
    {
        public int LayerproductId { get; set; }
        public int ProductId { get; set; }
        public int LayerId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}