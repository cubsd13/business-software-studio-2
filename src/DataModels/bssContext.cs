﻿using Microsoft.EntityFrameworkCore;

namespace SBA.DataModels
{
    public partial class bssContext : DbContext
    {
        public bssContext()
        {
        }

        public bssContext(DbContextOptions<bssContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Batch> Batch { get; set; }
        public virtual DbSet<Box> Box { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Layer> Layer { get; set; }
        public virtual DbSet<Layerproduct> Layerproduct { get; set; }
        public virtual DbSet<Logisticbox> Logisticbox { get; set; }
        public virtual DbSet<Logisticinfo> Logisticinfo { get; set; }
        public virtual DbSet<Master> Master { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Ordersdetail> Ordersdetail { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Resultbox> Resultbox { get; set; }
        public virtual DbSet<Resultordering> Resultordering { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;password=password;database=bss", x => x.ServerVersion("10.4.11-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Batch>(entity =>
            {
                entity.ToTable("batch");

                entity.HasComment("ข้อมูลแบช");

                entity.HasIndex(e => e.LogisticId)
                    .HasName("fk_batch_logisticId");

                entity.Property(e => e.BatchId)
                    .HasColumnName("batchId")
                    .HasColumnType("int(11)")
                    .HasComment("รหัสแบช");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime")
                    .HasComment("เวลาตัดรอบ");

                entity.Property(e => e.LogisticId)
                    .HasColumnName("logisticId")
                    .HasColumnType("int(11)")
                    .HasComment("รหัสบริษัทขนส่ง");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(11)")
                    .HasComment("ลำดับ");
            });

            modelBuilder.Entity<Box>(entity =>
            {
                entity.ToTable("box");

                entity.HasComment("ข้อมูลกล่อง");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสกล่อง");

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasComment("ความสูงกล่อง");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasComment("ความยาวกล่อง");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("text")
                    .HasComment("ชื่อกล่อง")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasComment("ความกว้างกล่อง");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.HasComment("ข้อมูลหมวดหมู่สินค้า");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("categoryId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสหมวดหมู่สินค้า");

                entity.Property(e => e.Categoryname)
                    .IsRequired()
                    .HasColumnName("categoryname")
                    .HasColumnType("text")
                    .HasComment("ชื่อหมวดหมู่สินค้า")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });

            modelBuilder.Entity<Layer>(entity =>
            {
                entity.ToTable("layer");

                entity.HasComment("ข้อมูลเลเยอร์");

                entity.HasIndex(e => e.ResultboxId)
                    .HasName("fk_layer_resultboxId");

                entity.Property(e => e.LayerId)
                    .HasColumnName("layerId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสเลเยอร์");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasComment("ความยาวเลเยอร์");

                entity.Property(e => e.ResultboxId)
                    .HasColumnName("resultboxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสผลลัพธ์การจัดกล่อง");

                entity.Property(e => e.SeqNumber)
                    .HasColumnName("seqNumber")
                    .HasColumnType("int(10)")
                    .HasComment("ลําดับของเลเยอร์ในกล่อง");

                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasComment("ความกว้างเลเยอร์");
            });

            modelBuilder.Entity<Layerproduct>(entity =>
            {
                entity.ToTable("layerproduct");

                entity.HasComment("ข้อมูลเลเยอร์ สินค้า");

                entity.HasIndex(e => e.LayerId)
                    .HasName("fk_layerproduct_layertId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("PRIMARY2")
                    .IsUnique();

                entity.Property(e => e.LayerproductId)
                    .HasColumnName("layerproductId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสเลเยอร์สินค้า");

                entity.Property(e => e.LayerId)
                    .HasColumnName("layerId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสเลเยอร์");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสสินค้า");

                entity.Property(e => e.X)
                    .HasColumnName("x")
                    .HasColumnType("int(20)")
                    .HasComment("ตําแหน่ง x ของสินค้าที่อยู่ในเลเยอร");

                entity.Property(e => e.Y)
                    .HasColumnName("y")
                    .HasColumnType("int(20)")
                    .HasComment("ตําแหน่ง y ของสินค้าที่อยู่ในเลเยอร์");
            });

            modelBuilder.Entity<Logisticbox>(entity =>
            {
                entity.ToTable("logisticbox");

                entity.HasComment("ข้อมูลกล่องของบริษัทขนส่ง");

                entity.HasIndex(e => e.BoxId)
                    .HasName("fk_logisticbox_boxId");

                entity.HasIndex(e => e.LogisticId)
                    .HasName("fk_logisticbox_logisticId");

                entity.Property(e => e.LogisticboxId)
                    .HasColumnName("logisticboxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสข้อมูลกล่องของขนส่ง");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสกล่อง");

                entity.Property(e => e.LogisticId)
                    .HasColumnName("logisticId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสบริษัทขนส่ง");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasComment("ราคาค่าขนส่ง");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasComment("นํ้าหนักสูงสุด");
            });

            modelBuilder.Entity<Logisticinfo>(entity =>
            {
                entity.HasKey(e => e.LogisticId)
                    .HasName("PRIMARY");

                entity.ToTable("logisticinfo");

                entity.HasComment("ข้อมูลบริษัทขนส่ง");

                entity.Property(e => e.LogisticId)
                    .HasColumnName("logisticId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสบริษัทขนส่ง");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("text")
                    .HasComment("ชื่อบริษัทขนส่ง")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasColumnType("text")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("เบอร์ติดต่อขนส่ง")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });
            modelBuilder.Entity<Master>(entity =>
          {
              entity.ToTable("master");

              entity.HasComment("ข้อมูลมาสเตอร์");

              entity.Property(e => e.Id)
                  .HasColumnName("id")
                  .HasColumnType("int(10)")
                  .HasComment("รหัสข้อมูล");

              entity.Property(e => e.Key)
                  .IsRequired()
                  .HasColumnName("key")
                  .HasColumnType("text")
                  .HasComment("โค้ดข้อมูล")
                  .HasCharSet("utf8")
                  .HasCollation("utf8_unicode_ci");

              entity.Property(e => e.Value)
                  .IsRequired()
                  .HasColumnName("value")
                  .HasColumnType("text")
                  .HasComment("ค่าข้อมูล")
                  .HasCharSet("utf8")
                  .HasCollation("utf8_unicode_ci");
          });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.ToTable("orders");

                entity.HasComment("ข้อมูลคําสั่งขาย");

                entity.Property(e => e.OrdersId)
                    .HasColumnName("ordersId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสคําสั่งขาย");

                entity.Property(e => e.CustomerAddress)
                    .IsRequired()
                    .HasColumnName("customerAddress")
                    .HasColumnType("text")
                    .HasComment("ที่อยู่จัดส่ง")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasColumnName("customerName")
                    .HasColumnType("text")
                    .HasComment("ชื่อลูกค้า")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date")
                    .HasComment("วันที่คําสั่งขาย");

                entity.Property(e => e.OrdersNumber)
                    .IsRequired()
                    .HasColumnName("ordersNumber")
                    .HasColumnType("text")
                    .HasComment("เลขที่คําสั่งขาย")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasColumnType("text")
                    .HasComment("สถานะคำสั่งขาย")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });

            modelBuilder.Entity<Ordersdetail>(entity =>
            {
                entity.ToTable("ordersdetail");

                entity.HasComment("ข้อมูลรายละเอียดคำสั่งซื้อ");

                entity.HasIndex(e => e.OrdersId)
                    .HasName("fk_ordersdetail_ordersId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk_ordersdetail_productid");

                entity.Property(e => e.OrdersdetailId)
                    .HasColumnName("ordersdetailId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสรายละเอียดคําสั่งขาย");

                entity.Property(e => e.OrdersId)
                    .HasColumnName("ordersId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสคําสั่งขาย");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสสินค้า");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)")
                    .HasComment("จํานวนสินค้า");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.HasComment("ข้อมูลสินค้า");

                entity.HasIndex(e => e.CategoryId)
                    .HasName("fk_product_categoryId");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสสินค้า");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("categoryId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสหมวดหมู่สินค้า");

                entity.Property(e => e.FrontView)
                    .HasColumnName("frontView")
                    .HasComment("วางด้าน Front View ลงกล่องได้ หรือไม่");

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasComment("ความสูงสินค้า");

                entity.Property(e => e.IsArranged)
                    .HasColumnName("isArranged")
                    .HasComment("ถูกนำไปคำนวณการจัดเรียง หรือไม่");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasComment("ความยาวสินค้า");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("text")
                    .HasComment("ชื่อสินค้า")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.SideView)
                    .HasColumnName("sideView")
                    .HasComment("วางด้าน Side View ลงกล่องได้ หรือไม่");

                entity.Property(e => e.TopView)
                    .HasColumnName("topView")
                    .HasComment("วางด้าน Top View ลงกล่องได้ หรือไม่");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasComment("นํ้าหนักสินค้า");

                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasComment("ความกว้างสินค้า");
            });

            modelBuilder.Entity<Resultbox>(entity =>
            {
                entity.ToTable("resultbox");

                entity.HasComment("ข้อมูลผลลัพธ์การจัดกล่อง");

                entity.HasIndex(e => e.LogisticboxId)
                    .HasName("fk_resultbox_logisticboxId");

                entity.HasIndex(e => e.ResultorderingId)
                    .HasName("fk_resultbox_resultorderingId");

                entity.Property(e => e.ResultboxId)
                    .HasColumnName("resultboxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสผลลัพธ์การจัดกล่อง");

                entity.Property(e => e.LogisticboxId)
                    .HasColumnName("logisticboxId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสข้อมูลกล่องของขนส่ง");

                entity.Property(e => e.ResultorderingId)
                    .HasColumnName("resultorderingId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสรายการผลลัพธ์ของคําสั่งขาย");
            });

            modelBuilder.Entity<Resultordering>(entity =>
            {
                entity.ToTable("resultordering");

                entity.HasComment("ข้อมูลรายการผลลัพธ์ของคําสั่งขาย");

                entity.HasIndex(e => e.BatchId)
                    .HasName("fk_resultordering_batchId");

                entity.HasIndex(e => e.OrdersId)
                    .HasName("fk_resultordering_ordersId");

                entity.Property(e => e.ResultorderingId)
                    .HasColumnName("resultorderingId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสรายการผลลัพธ์ของคําสั่งขาย");

                entity.Property(e => e.BatchId)
                    .HasColumnName("batchId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสแบช");

                entity.Property(e => e.OrdersId)
                    .HasColumnName("ordersId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสคําสั่งขาย");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasComment("ข้อมูลผู้ใช้งาน");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(10)")
                    .HasComment("รหัสผู้ใช้งาน");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("text")
                    .HasComment("รหัสผ่านเข้าใช้งาน")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("text")
                    .HasComment("ชื่อผู้ใช้งาน")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}