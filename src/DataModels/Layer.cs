﻿namespace SBA.DataModels
{
    public partial class Layer
    {
        public int LayerId { get; set; }
        public int ResultboxId { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public int SeqNumber { get; set; }
    }
}