﻿namespace SBA.DataModels
{
    public partial class Master
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}