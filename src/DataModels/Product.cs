using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Product
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public bool FrontView { get; set; }
        public bool TopView { get; set; }
        public bool SideView { get; set; }
        public bool IsArranged { get; set; }

        [NotMapped]
        public string Categoryname { get; set; }
    }
}