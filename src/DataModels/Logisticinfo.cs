using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Logisticinfo
    {
        public int LogisticId { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }

        [NotMapped]
        public int boxAmount { get; set; }
    }
}