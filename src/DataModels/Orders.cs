using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Orders
    {
        public Orders()
        {
            OrdersDetails = new List<Ordersdetail>();
        }

        public int OrdersId { get; set; }
        public string OrdersNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public List<Ordersdetail> OrdersDetails { get; set; }

        [NotMapped]
        public int TotalProductQty { get; set; }
    }

    public static class OrderStatus
    {
        public static string NEW = "NEW";
        public static string EDIT = "EDIT";
        public static String ARRANGED = "ARRANGED";
        public static String PRINT = "PRINT";
        public static String DELETE = "DELETE";
    }
}