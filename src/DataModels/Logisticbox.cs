﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Logisticbox
    {
        public int LogisticboxId { get; set; }
        public int LogisticId { get; set; }
        public int BoxId { get; set; }
        public double Price { get; set; }
        public double Weight { get; set; }

        [NotMapped]
        public string BoxName { get; set; }

        [NotMapped]
        public double Length { get; set; }

        [NotMapped]
        public double Width { get; set; }

        [NotMapped]
        public double Height { get; set; }
    }
}