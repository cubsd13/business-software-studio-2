using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Batch
    {
        public int BatchId { get; set; }
        public DateTime Datetime { get; set; }
        public int LogisticId { get; set; }
        public int Sequence { get; set; }

        [NotMapped]
        public string LogisticName { get; set; }

        [NotMapped]
        public List<Algorithm.Order> ResultOrder { get; set; }

        [NotMapped]
        public double TotalPrice { get; set; }

        [NotMapped]
        public int TotalOrder { get; set; }

        public Batch()
        {
            ResultOrder = new List<Algorithm.Order>();
        }
    }
}