namespace SBA.DataModels
{
    public class ArrangementResultReport : Algorithm.Order
    {
        public int LogisticId { get; set; }
        public string LogisticName { get; set; }

        public ArrangementResultReport(int id) : base(id)
        {
        }
    }
}