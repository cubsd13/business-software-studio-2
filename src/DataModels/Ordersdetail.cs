using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.DataModels
{
    public partial class Ordersdetail
    {
        public int OrdersdetailId { get; set; }
        public int ProductId { get; set; }
        public int OrdersId { get; set; }
        public int Qty { get; set; }

        [NotMapped]
        public string ProductName { get; set; }
    }
}