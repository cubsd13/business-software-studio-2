namespace SBA.DataModels
{
    public partial class Category
    {
        public int CategoryId { get; set; }
        public string Categoryname { get; set; }
    }
}