﻿namespace SBA.DataModels
{
    public partial class Box
    {
        public int BoxId { get; set; }
        public string Name { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public double Height { get; set; }
    }
}