using System.Linq;

namespace SBA.DataModels
{
    public partial class ShippingAddressReport
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerZipcode { get; set; }
        public string CustomerPhone { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyZipcode { get; set; }
        public string CompanyPhone { get; set; }

        public void SetCustomerAddress(string addr)
        {
            CustomerZipcode = addr.Trim().Substring(addr.Trim().Count() - 5, 5);
            CustomerAddress = addr.Trim().Substring(0, addr.Trim().Count() - 5);
        }

        public void SetCustomerName(string name)
        {
            CustomerName = name;
        }
    }
}