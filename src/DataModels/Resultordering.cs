﻿namespace SBA.DataModels
{
    public partial class Resultordering
    {
        public int ResultorderingId { get; set; }
        public int BatchId { get; set; }
        public int OrdersId { get; set; }
    }
}