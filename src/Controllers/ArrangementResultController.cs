using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.Algorithm;
using SBA.DataModels;
using System.Collections.Generic;

namespace SBA.Controllers
{
    [Authorize]
    public class ArrangementResultController : Controller
    {
        public ArrangementResultController(bssContext context)
        {
        }

        public IActionResult ArrangeBox(List<int> orderIds)
        {
            var orderArr = new OrderArrangement();
            var result = orderArr.GetResult(orderIds);
            return View("Index", result);
        }

        public IActionResult GetResultByOrderResult(int orderId, int logisticId)
        {
            var obj = new OrderArrangement();
            var result = obj.GetResultByOrderResult(orderId, logisticId);
            return Json(result);
        }
    }
}