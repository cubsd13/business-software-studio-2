using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;

namespace SBA.Controllers
{
    [Authorize]
    public class LogisticinfoController : Controller
    {
        private readonly LogisticinfoRepository _logisticInfoRepo;
        private readonly BoxRepository _boxRepo;

        public LogisticinfoController(bssContext context)
        {
            _logisticInfoRepo = new LogisticinfoRepository(context);
            _boxRepo = new BoxRepository(context);
        }

        public IActionResult Index()
        {
            var result = _logisticInfoRepo.GetAllLogisticinfo();
            return View();
        }

        public IActionResult GetAllLogisticsInfo()
        {
            return Json(new { data = _logisticInfoRepo.GetAllLogisticinfo() });
        }

        public IActionResult GetLogisticboxById(int id)
        {
            return Json(new { data = _logisticInfoRepo.GetLogisticboxById(id) });
        }

        public IActionResult GetAllBox()
        {
            return Json(new { data = _boxRepo.GetAllBoxes() });
        }

        public IActionResult UpsertLogisticInfo(Logisticinfo obj)
        {
            var result = false;
            var method = "";
            if (obj.LogisticId > 0)
            {
                result = _logisticInfoRepo.UpdateLogisticInfo(obj);
                method = "Update Logistic Info";
            }
            else
            {
                result = _logisticInfoRepo.AddLogisticInfo(obj);
                method = "Add Logistic Info";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return RedirectToAction("Index");
        }

        public IActionResult UpsertLogisticBox(Logisticbox obj)
        {
            var result = false;
            var method = "";
            if (obj.LogisticboxId > 0)
            {
                result = _boxRepo.UpdateBox(obj);
                method = "Update Logistic Box";
            }
            else
            {
                result = _boxRepo.AddBox(obj);
                method = "Add Logistic Box";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return RedirectToAction("Index");
        }
    }
}