using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;

namespace SBA.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly CategoryRepository _categoryRepo;

        public CategoryController(bssContext context)
        {
            _categoryRepo = new CategoryRepository(context);
        }

        public IActionResult Index()
        {
            var result = _categoryRepo.GetAllCategory();

            return View(result);
        }

        public IActionResult UpsertCategory(Category obj)
        {
            var result = false;
            var method = "";

            if (obj.CategoryId > 0)
            {
                result = _categoryRepo.UpdateCategory(obj);
                method = "Update Data";
            }
            else
            {
                result = _categoryRepo.AddCategory(obj);
                method = "Add Data";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return RedirectToAction("Index");
        }

        public IActionResult GetAllCategory()
        {
            var result = _categoryRepo.GetAllCategory();
            return Json(new { data = result });
        }
    }
}