using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.Algorithm;
using SBA.DataModels;
using SBA.Repositories;
using System.Collections.Generic;

namespace SBA.Controllers
{
    [Authorize]
    public class BatchController : Controller
    {
        private readonly BatchRepository _batchRepo;

        public BatchController(bssContext context)
        {
            _batchRepo = new BatchRepository(context);
        }
        public IActionResult Index()
        {
            var result = _batchRepo.GetAllBatchs();
            return View(result);
        }
        public IActionResult ViewArrangeBox(int BatchId)
        {
            List<int> orderIds = _batchRepo.GetOrderListByBatchId(BatchId);
            var orderArr = new OrderArrangement();
            var result = orderArr.GetResult(orderIds);
            return View("ViewArrangeBox", result);
        }
    }
}