using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;
using System;

namespace SBA.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly OrderRepository _orderRepo;

        public OrderController(bssContext context)
        {
            _orderRepo = new OrderRepository(context);
        }

        public IActionResult Index()
        {
            var result = _orderRepo.GetAllOrders();
            return View(result);
        }

        public IActionResult AddOrder()
        {
            var obj = new Orders();
            obj.Date = DateTime.Now;
            return View("AddOrder", obj);
        }

        public IActionResult GetOrderByOrderNumber(string ordersnumber)
        {
            var obj = _orderRepo.GetOrderByOrderNumber(ordersnumber);
            if (obj != null)
            {
                return View("AddOrder", obj);
            }
            else
            {
                return View("AddOrder", new Orders() { Date = DateTime.Now, OrdersNumber = ordersnumber });
            }
        }

        public IActionResult GetOrderById(int id)
        {
            var obj = _orderRepo.GetOrderById(id);
            if (obj == null)
            {
                return AddOrder();
            }
            else
            {
                return View("AddOrder", obj);
            }
        }

        public IActionResult UpsertOrder(Orders obj)
        {
            var result = false;
            var method = "";

            if (obj.OrdersId > 0)
            {
                result = _orderRepo.EditOrder(obj);
                method = "Update Order";
            }
            else
            {
                result = _orderRepo.AddOrder(obj);
                method = "Add Order";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return GetOrderById(obj.OrdersId);
        }

        public IActionResult UpsertOrderDetail(Ordersdetail obj)
        {
            var result = false;
            var method = "";

            if (obj.OrdersdetailId > 0)
            {
                result = _orderRepo.EditOrderDetail(obj);
                method = "Update OrderDetail";
            }
            else
            {
                result = _orderRepo.AddOrderDetail(obj);
                method = "Add OrderDetail ";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return GetOrderById(obj.OrdersId);
        }

        public IActionResult DeleteOrder(int ordersid)
        {
            var result = false;
            var method = "Delete Order";

            if (ordersid > 0)
            {
                result = _orderRepo.DeleteOrder(ordersid);
            }

            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return AddOrder();
        }

        public IActionResult DeleteOrderdetail(int ordersid, int ordersdetailid)
        {
            var result = false;
            var method = "Delete Orderdetail";

            if (ordersdetailid > 0)
            {
                result = _orderRepo.DeleteOrderdetail(ordersdetailid);
            }

            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return GetOrderById(ordersid);
        }
    }
}