using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;

namespace src.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly UserRepository _userRepo;

        public UserController(bssContext context)
        {
            _userRepo = new UserRepository(context);
        }

        public IActionResult Index()
        {
            var result = _userRepo.GetAllUsers();
            return View(result);
        }

        [HttpPost]
        public IActionResult Save(User userModel)
        {
            try
            {
                bool result = false;

                if (userModel.UserId > 0)
                    result = _userRepo.UpdateUser(userModel);
                else
                    result = _userRepo.CreateUser(userModel);

                return RedirectToAction("Index");
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}