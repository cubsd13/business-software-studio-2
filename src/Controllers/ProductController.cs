using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;

namespace SBA.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly ProductRepository _productRepo;

        public ProductController(bssContext context)
        {
            _productRepo = new ProductRepository(context);
        }

        public IActionResult Index()
        {
            var result = _productRepo.GetAllProduct();

            return View(result);
        }

        public IActionResult UpsertProduct(Product obj)
        {
            var result = false;
            var method = "";

            if (obj.ProductId > 0)
            {
                result = _productRepo.UpdateProduct(obj);
                method = "Update Product";
            }
            else
            {
                result = _productRepo.AddProduct(obj);
                method = "Add Product";
            }
            var resultMessage = result ? "Success" : "Fail";
            TempData["ActionResult"] = result;
            TempData["ActionResultMessage"] = $"{method} {resultMessage} ";
            return RedirectToAction("Index");
        }

        public IActionResult GetAllProduct()
        {
            return Json(new { data = _productRepo.GetAllProduct() });
        }

        public IActionResult GetProductIsArranged()
        {
            return Json(new { data = _productRepo.GetProductIsArranged() });
        }

        public IActionResult GetProductNotArranged()
        {
            return Json(new { data = _productRepo.GetProductNotArranged() });
        }
    }
}