using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SBA.Algorithm;
using SBA.DataModels;
using SBA.Repositories;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace SBA.Controllers
{
    [Authorize]
    public class DocumentController : Controller
    {
        private readonly MasterRepository _masterRepo;
        private readonly OrderRepository _orderRepo;

        public DocumentController(bssContext _context)
        {
            _orderRepo = new OrderRepository(_context);
            _masterRepo = new MasterRepository(_context);
        }

        public IActionResult OrderProductNotArranged()
        {
            return View("OrderProductNotArranged", new Orders());
        }

        public IActionResult GetOrderByOrderNumber(string ordersnumber)
        {
            var obj = _orderRepo.GetOrderAllByOrderNumber(ordersnumber);
            if (obj == null)
            {
                return View("OrderProductNotArranged", new Orders() { OrdersNumber = ordersnumber });
            }
            else
            {
                return View("OrderProductNotArranged", obj);
            }
        }
        public IActionResult GetOrderDetailsList()
        {
            var obj = new List<Ordersdetail>();
            return Json(new { data = obj });
        }
        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult ShippingAddressReport(int orderId)
        {
            HttpContext.JsReportFeature()
                       .Configure((req) => { if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) { req.Options.Base = "http://bss2-nginx"; } })
                       .Recipe(Recipe.ChromePdf);
            return View("ShippingAddressReport", _masterRepo.GetShippingAddress(orderId));
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult ArrangementResultReport(int orderId, int logisticId)
        {
            HttpContext.JsReportFeature()
                       .Configure((req) => { if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) { req.Options.Base = "http://bss2.test"; } })
                       .Recipe(Recipe.ChromePdf);
            return View("ArrangementResultReport", (new OrderArrangement()).GetArrangementResultReport(orderId, logisticId));
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult CoverBoxReport(int orderId, int logisticId)
        {
            HttpContext.JsReportFeature()
                       .Configure((req) => { if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) { req.Options.Base = "http://bss2.test"; } })
                       .Recipe(Recipe.ChromePdf);
            return View("CoverBoxReport", (new OrderArrangement()).GetCoverBoxReport(orderId, logisticId));
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult CoverBoxReportNotArranged(Orders order)
        {
            HttpContext.JsReportFeature()
                       .Configure((req) => { if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) { req.Options.Base = "http://bss2.test"; } })
                       .Recipe(Recipe.ChromePdf);
            return View("CoverBoxReportNotArranged", order);
        }
    }
}