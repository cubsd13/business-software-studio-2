using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SBA.DataModels;
using SBA.Repositories;
using src.ViewModels;
using System.Security.Claims;

namespace SBA.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserRepository _UserRepo;
        private readonly ISession Session;

        public LoginController(IHttpContextAccessor httpContextAccessor, bssContext context)
        {
            this.Session = httpContextAccessor.HttpContext.Session;
            _UserRepo = new UserRepository(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Login");
        }

        [HttpPost]
        public IActionResult Index(UserLoginModel loginModel)
        {
            if (string.IsNullOrEmpty(loginModel.Username) || string.IsNullOrEmpty(loginModel.Password))
            {
                ViewData["Error"] = "Invalid username or password.";
                return View(loginModel);
            }
            User login = _UserRepo.Login(loginModel);
            if (login == null)
            {
                ViewData["Error"] = "Invalid username or password.";
                return View(loginModel);
            }
            else
            {
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, login.Username)},
                CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var signIn = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                HttpContext.Session.SetString("LOGIN_USER", login.Username);
                return RedirectToAction("Index", "Home");
            }
        }
    }
}