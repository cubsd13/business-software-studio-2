using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SBA.Algorithm
{
    public class Box
    {
        public Guid Guid { get; } = Guid.NewGuid();
        public int LogisticId { get; set; }
        public int LogisticboxId { get; set; }
        public double Price { get; set; }
        public string BoxName { get; set; }

        private int length; // X
        private int width;  // Y
        private int height; // Z
        private int weight;

        public List<Layer> layers = new List<Layer>();

        public int totalBlock
        {
            get { return this.layers.Sum(l => l.totalBlock); }
        }

        public int totalLayer
        {
            get { return this.layers.Count(); }
        }

        private Layer Layer
        {
            get { return layers.Last(); }
        }

        public int CurrentWeight
        {
            get { return this.layers.Sum(layer => layer.CurrentWeight); }
        }

        public int CurrentHeight
        {
            get { return (this.layers.Count <= 0) ? 0 : this.layers.Sum(layer => layer.CurrentHeightMedian); }
        }

        public bool isWeightValid
        {
            get { return (this.CurrentWeight >= this.weight) ? false : true; }
        }

        public int Weight
        {
            get { return this.weight; }
        }

        public int Height
        {
            get { return this.height; }
        }

        public int WeightAvailable
        {
            get { return this.weight - this.CurrentWeight; }
        }

        public bool isHeightValid
        {
            get { return (this.CurrentHeight > this.height) ? false : true; }
        }

        public bool isHeightLayerValid(int height)
        {
            return ((this.CurrentHeight + height) <= this.height) ? true : false;
        }

        public Box(int length, int width, int height, int weight)
        {
            this.length = length;
            this.width = width;
            this.height = height;
            this.weight = weight;
            this.NewLayer();
        }

        public bool NewLayer(int minimumHeightOfBlocks = 1)
        {
            if (!this.isHeightLayerValid(minimumHeightOfBlocks) || !this.isWeightValid)
            {
                return false;
            }

            List<Block> blocks = new List<Block>();

            if (this.totalLayer > 0)
            {
                blocks = this.Layer.Blocks.Where(b => b.Height > this.Layer.CurrentHeightMedian).ToList();
                int height = this.Layer.CurrentHeightMedian;
            }

            this.layers.Add(new Layer(this.length, this.width, this.height, this.WeightAvailable));

            if (this.totalLayer > 0)
            {
                foreach (Block block in blocks)
                {
                    Block b = new Block();
                    b.Color = Color.Black;
                    b.Length = block.Length;
                    b.Width = block.Width;
                    b.Height = block.Height - height;
                    b.X = block.X;
                    b.Y = block.Y;
                    this.Layer.blocks.Add(b);

                    block.Height = height;
                }
            }

            return true;
        }

        private void debug()
        {
            if (!this.isHeightValid)
            {
                Console.WriteLine("Box:isHeightValid:false");
            }

            if (!this.isWeightValid)
            {
                Console.WriteLine("Box:isWeightValid:false");
            }
        }

        public bool AddBlock(Block block)
        {
            if (!this.isHeightValid || !this.isWeightValid)
            {
                this.debug();
                return false;
            }

            return this.Layer.AddBlock(block);
        }

        public List<Block> TotalProduct { get { return GetTotalProduct().GroupBy(p => p.ProductId).Select(x => x.First()).OrderBy(p => p.Name).ToList(); } }

        private List<Block> GetTotalProduct()
        {
            var result = new List<Block>();
            this.layers.ForEach(p => result.AddRange(p.blocks));
            return result.Where(p => p.ProductId != 0).ToList();
        }

        public int GetProductAmountById(int productId)
        {
            return GetTotalProduct().Where(p => p.ProductId == productId).Count();
        }
    }
}