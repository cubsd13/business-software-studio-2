﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace SBA.Algorithm
{
    public class xColor
    {
        private Dictionary<int, Color> color = new Dictionary<int, Color>();

        public Color Get(int reference)
        {
            if (this.color.ContainsKey(reference))
            {
                return this.color[reference];
            }

            return this.Set(reference);
        }

        private Color Set(int reference)
        {
            Color color = this.Random(reference);
            this.color.Add(reference, color);
            return color;
        }

        private Color Random(int reference)
        {
            if (reference == 0) return Color.Black;

            var random = new Random(reference);

            return Color.FromArgb(
                random.Next(128, 255),
                random.Next(128, 255),
                random.Next(128, 255)
            );
        }
    }
}