﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.Algorithm.Models
{
    public partial class Order
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }

        [NotMapped]
        public int Volume
        {
            get { return this.Width * this.Length * this.Height; }
        }
    }
}