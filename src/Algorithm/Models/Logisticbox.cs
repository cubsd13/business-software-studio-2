﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SBA.Algorithm.Models
{
    public partial class Logisticbox
    {
        public int LogisticboxId { get; set; }
        public int LogisticId { get; set; }
        public int BoxId { get; set; }
        public double Price { get; set; }
        public int Weight { get; set; }

        [NotMapped]
        public string BoxName { get; set; }

        [NotMapped]
        public int Length { get; set; }

        [NotMapped]
        public int Width { get; set; }

        [NotMapped]
        public int Height { get; set; }

        [NotMapped]
        public int Volume
        {
            get { return this.Width * this.Length * this.Height; }
        }

        public int VolumeXY
        {
            get { return this.Width * this.Length; }
        }
    }
}