using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Algorithm
{
    public class Order
    {
        private int _id;
        public int Id { get { return orderId; } }
        public String CustomerName { get { return customerName; } }
        public String OrderNumber { get { return orderNumber; } }
        private List<Models.Logisticbox> logisticBoxes = new List<Models.Logisticbox>();

        private int orderId = 0;
        private string orderNumber = string.Empty;
        private string customerName = string.Empty;

        public void SetOrderId(int orderId)
        {
            this.orderId = orderId;
        }

        public void SetOrderNumber(String orderNumber)
        {
            this.orderNumber = orderNumber;
        }

        public void SetCustomerName(String customerName)
        {
            this.customerName = customerName;
        }

        private List<Block> blocks = new List<Block>();

        public List<Box> ResultBoxes = new List<Box>();
        private xColor xColor = new xColor();

        public int TotalBlocks
        {
            get { return this.ResultBoxes.Sum(p => p.totalBlock); }
        }

        public double TotalPrice
        {
            get { return ResultBoxes.Sum(p => p.Price); }
        }

        public int Weight
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Sum(block => block.Weight); }
        }

        public int Volume
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Sum(block => block.Volume); }
        }

        public int VolumeXY
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Sum(block => block.VolumeXY); }
        }

        public int HighestOfBlock
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Max(block => block.Height); }
        }

        public int ShortestOfBlock
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Min(block => block.Height); }
        }

        public List<Block> DebugBlocks { get { return blocks; } }

        public Order(int OrderId)
        {
            this._id = OrderId;
            this.FetchOrder();
        }

        public void SetBoxes(List<Models.Logisticbox> logisticBoxes)
        {
            this.logisticBoxes = logisticBoxes.OrderByDescending(box => box.Volume).ToList();
        }

        public void FetchOrder()
        {
            this.blocks = new List<Block>();
            Algorithm.Block block;
            Algorithm.DataFeed.Order order = new Algorithm.DataFeed.Order(this._id);

            foreach (Algorithm.Models.Order item in order.GetItems())
            {
                for (int i = 0; i < item.Qty; i++)
                {
                    block = new Block();
                    block.Color = this.xColor.Get(item.ProductId);
                    block.OrderId = item.OrderId;
                    block.ProductId = item.ProductId;
                    block.Name = item.Name;
                    block.Length = item.Length;
                    block.Width = item.Width;
                    block.Height = item.Height;
                    block.Weight = item.Weight;
                    this.blocks.Add(block);
                }
            }
        }

        private Models.Logisticbox NextBox()
        {
            Models.Logisticbox res = this.logisticBoxes.First();

            foreach (Models.Logisticbox logisticBox in this.logisticBoxes)
            {
                if ((logisticBox.VolumeXY <= this.VolumeXY) || (logisticBox.Weight <= this.Weight) || (logisticBox.Height <= this.HighestOfBlock))
                {
                    return res;
                }

                res = logisticBox;
            }

            return res;
        }

        private Box ConvertAsBox(Models.Logisticbox logisticBox)
        {
            Box box = new Box(logisticBox.Length, logisticBox.Width, logisticBox.Height, logisticBox.Weight);
            box.LogisticboxId = logisticBox.LogisticboxId;
            box.LogisticId = logisticBox.LogisticId;
            box.Price = logisticBox.Price;
            box.BoxName = logisticBox.BoxName;

            return box;
        }

        public bool Try2Place(Block block, Box box)
        {
            if (block.Length > block.Width) block.SwapXY();

            if (box.AddBlock(block))
            {
                this.blocks.Remove(block);
                return true;
            }

            block.SwapXY();

            if (box.AddBlock(block))
            {
                this.blocks.Remove(block);
                return true;
            }

            return false;
        }

        private void ConditionSorting(Box box)
        {
            this.blocks = this.blocks.OrderBy(b => b.Weight).ThenBy(b => b.Height).ToList();

            if (box.isHeightLayerValid(this.ShortestOfBlock) && box.isHeightLayerValid(this.HighestOfBlock))
            {
                this.blocks = this.blocks.OrderByDescending(b => b.Weight).ToList();
            }
        }

        public bool Arrangement()
        {
            if (this.logisticBoxes.Count() <= 0)
            {
                return false;
            }

            Box box = this.ConvertAsBox(this.NextBox());

            this.blocks = this.blocks.OrderByDescending(b => b.Weight).ToList();
            Block lastBlock = this.blocks.Last();

            while (true)
            {
                if (this.blocks.Count <= 0) { break; }

                Block currentBlock = this.blocks.First();
                Boolean isLastBlock = (currentBlock.Equals(lastBlock)) ? true : false;
                if (this.Try2Place(currentBlock, box))
                {
                    if (this.blocks.Count <= 0) { break; }
                    lastBlock = this.blocks.Last();
                    continue;
                }

                if (isLastBlock && !box.NewLayer(this.ShortestOfBlock))
                {
                    this.ResultBoxes.Add(box);
                    return false;
                }
                else
                {
                    // Condition ตรงนี้จะแปลกๆ หน่อย ความหมายคือ ถ้า new layer ได้ ให้เรียงของตามข้อกำหนดใหม่ (resort item Q)
                    if (isLastBlock)
                    {
                        this.ConditionSorting(box);
                        lastBlock = this.blocks.Last();
                        continue;
                    }
                }

                // Pop and push into tailQ
                currentBlock = this.blocks.First();
                this.blocks.Remove(currentBlock);
                this.blocks.Add(currentBlock);
            }

            // จบแบบกล่องไม่สูงเกิน และน้ำหนักไม่เกิน
            this.ResultBoxes.Add(box);
            return true;
        }
    }
}