using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Algorithm
{
    public class Batch
    {
        public int Id { get; set; }
        public int LogisticId { get { return this.logisticId; } }
        public string LogisticName { get { return this.logisticName; } }
        public DateTime Datetime { get; }
        public int Sequence { get { return this.sequence; } }
        public List<Order> Order { get { return this.orders; } }

        private List<Order> orders = new List<Order>();
        private int logisticId = 0;
        private string logisticName = string.Empty;
        private int sequence = 0;

        public double TotalPrice
        {
            get { return orders.Sum(p => p.TotalPrice); }
        }

        public Batch()
        {
            this.Datetime = DateTime.Now;
        }

        public void SetLogisticId(int logisticId)
        {
            this.logisticId = logisticId;
        }

        public void SetLogisticName(String logisticName)
        {
            this.logisticName = logisticName;
        }

        public void SetSequence(int sequence)
        {
            this.sequence = sequence;
        }

        public void SetDefaultBoxes(List<Models.Logisticbox> logisticBoxes)
        {
            foreach (Algorithm.Order order in this.orders)
            {
                order.FetchOrder();
                order.SetBoxes(logisticBoxes);
                while (!order.Arrangement()) { }
            }
        }

        public void AddOrder(Algorithm.Order order)
        {
            this.orders.Add(order);
        }

        public double GetPriceOfLogistic()
        {
            return TotalPrice;
        }

        public List<int> GetOrderIds()
        {
            List<int> results = new List<int>();

            foreach (Algorithm.Order order in this.orders)
            {
                results.Add(order.Id);
            }

            return results;
        }

        public List<Box> GetBoxesByOrderId(int orderId)
        {
            Algorithm.Order order = this.GetOrdersById(orderId);

            if (this.LogisticId != 0)
            {
                return order.ResultBoxes.Where(b => b.LogisticboxId == this.LogisticId).ToList();
            }

            return order.ResultBoxes;
        }

        public Algorithm.Order GetOrdersById(int orderId)
        {
            Algorithm.Order order = (Order)this.orders.Where(o => o.Id == orderId).ToList().First();

            if (this.LogisticId != 0)
            {
                order.ResultBoxes = order.ResultBoxes.Where(b => b.LogisticboxId == this.LogisticId).ToList();
            }

            return order;
        }
    }
}