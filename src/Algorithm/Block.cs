using System;
using System.Drawing;
using System.Linq;

namespace SBA.Algorithm
{
    public class Block
    {
        public Guid Guid { get; } = Guid.NewGuid();
        public Color Color { get; set; } = Color.White;
        public int OrderId { get; set; } = 0;
        public int ProductId { get; set; } = 0;
        public string Name { get; set; } = "NA";
        public int Length { get; set; } // X
        public int Width { get; set; } // Y
        public int Height { get; set; } // Z
        public bool isSwap { get { return this.isswap; } }

        public string PositionFilpup
        {
            get
            {
                // ref: https://cubsd13.atlassian.net/jira/software/projects/BSS/boards/3?selectedIssue=BSS-252
                int[] dimension = { this.Length, this.Width, this.Height };
                if (dimension.Min() == dimension.Max()) return "Front View";
                if (dimension.Min() == this.Height) return "Front View";
                if (dimension.Max() == this.Height) return "Side View";
                return "Top View";
            }
        }

        public int Weight { get; set; } = 0;

        public int Volume
        {
            get { return this.Width * this.Length * this.Height; }
        }

        public int VolumeXY
        {
            get { return this.Width * this.Length; }
        }
        // Position
        public int X { get; set; } = 0;
        public int Y { get; set; } = 0;
        private bool isswap = false;
        public void SwapXY()
        {
            int length = this.Length;
            this.Length = this.Width;
            this.Width = length;
            this.isswap = true;
        }
    }
}