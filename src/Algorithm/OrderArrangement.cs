using SBA.Algorithm.DataFeed;
using SBA.DataModels;
using SBA.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Algorithm
{
    public class OrderArrangement
    {
        public Order GetResultByOrderResult(int orderId, int logisticId)
        {
            Algorithm.Order algoOrder = new Algorithm.Order(orderId);
            Algorithm.DataFeed.Boxes box = new Algorithm.DataFeed.Boxes();

            algoOrder.FetchOrder();
            algoOrder.SetBoxes(box.GetByLogisticId(logisticId));

            while (!algoOrder.Arrangement())
            {
                Console.WriteLine("NextBox!!");
            }

            return algoOrder;
        }

        public Algorithm.Batch GetResult(List<int> orderIds)
        {
            Program.cycleCount = 0;
            OrderRepository _orderRepo = new OrderRepository(new Context());
            BatchRepository _batchRepo = new BatchRepository(new Context());
            ResultOrderingRepository _resultOrderingRepo = new ResultOrderingRepository(new Context());

            var batchList = new List<Algorithm.Batch>();

            Algorithm.DataFeed.Boxes box = new Algorithm.DataFeed.Boxes();

            foreach (int logisticId in box.GetLogisticIds())
            {
                Algorithm.Batch batch = new Algorithm.Batch();
                foreach (var i in orderIds)
                {
                    Algorithm.Order order = new Algorithm.Order(i);
                    Orders orders = _orderRepo.GetOrderById(i);
                    order.SetOrderId(i);
                    order.SetOrderNumber(orders.OrdersNumber);
                    order.SetCustomerName(orders.CustomerName);
                    batch.AddOrder(order);
                }
                batch.SetLogisticId(logisticId);
                batch.SetDefaultBoxes(box.GetByLogisticId(logisticId));
                batchList.Add(batch);
            }
            var min = batchList.Min(p => p.TotalPrice);

            Algorithm.Batch batchResult = batchList.Where(p => p.TotalPrice == min).FirstOrDefault();

            LogisticinfoRepository _logisticInfoRepo = new LogisticinfoRepository(new Context());
            Logisticinfo logisticinfo = _logisticInfoRepo.GetLogisticInfobyId(batchResult.LogisticId);
            batchResult.SetLogisticName(logisticinfo.Name);

            Console.WriteLine("<<<<<<batchResult>>>>>>");
            Console.WriteLine("LogisticId: " + batchResult.LogisticId);
            Console.WriteLine("<<<<<<order list>>>>>>");

            if (_orderRepo.isArranged(orderIds) == false)
            {
                int batchSequence = _batchRepo.GetBatchIdSequence(DateTime.Now);
                DataModels.Batch tblBatch = new DataModels.Batch();
                tblBatch.Datetime = DateTime.Now;
                tblBatch.LogisticId = batchResult.LogisticId;
                tblBatch.Sequence = batchSequence;
                _batchRepo.AddBatch(tblBatch);
                foreach (var i in orderIds)
                {
                    DataModels.Resultordering tblResultOrdering = new DataModels.Resultordering();
                    tblResultOrdering.BatchId = _batchRepo.GetLastBatchId();
                    tblResultOrdering.OrdersId = i;
                    _resultOrderingRepo.AddResultOrdering(tblResultOrdering);
                    Orders orders = _orderRepo.GetOrderById(i);
                    _orderRepo.SetOrderStatusByOrdersNumber(orders.OrdersNumber);
                }
            }
            Console.WriteLine("Cycle Count: {0}", Program.cycleCount.ToString("#,###"));
            return batchResult;
        }

        public DataModels.ArrangementResultReport GetArrangementResultReport(int orderId, int logisticId)
        {
            OrderRepository _orderRepo = new OrderRepository(new Context());
            LogisticinfoRepository _logisticInfoRepo = new LogisticinfoRepository(new Context());
            var result = new ArrangementResultReport(orderId);
            result.FetchOrder();
            result.SetBoxes((new Algorithm.DataFeed.Boxes()).GetByLogisticId(logisticId));

            while (!result.Arrangement())
            {
                Console.WriteLine("NextBox!!");
            }
            Orders order = _orderRepo.GetOrderById(orderId);
            result.SetOrderId(orderId);
            result.SetOrderNumber(order.OrdersNumber);
            result.SetCustomerName(order.CustomerName);
            result.LogisticId = logisticId;
            result.LogisticName = _logisticInfoRepo.GetLogisticInfobyId(logisticId).Name;
            return result;
        }

        public DataModels.CoverBoxReport GetCoverBoxReport(int orderId, int logisticId)
        {
            OrderRepository _orderRepo = new OrderRepository(new Context());
            var result = new CoverBoxReport(orderId);
            result.FetchOrder();
            result.SetBoxes((new Algorithm.DataFeed.Boxes()).GetByLogisticId(logisticId));

            while (!result.Arrangement())
            {
                Console.WriteLine("NextBox!!");
            }
            Orders order = _orderRepo.GetOrderById(orderId);
            result.SetOrderId(orderId);
            result.SetOrderNumber(order.OrdersNumber);
            result.SetCustomerName(order.CustomerName);
            return result;
        }
    }
}