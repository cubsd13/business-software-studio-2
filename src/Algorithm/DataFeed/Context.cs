using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace SBA.Algorithm.DataFeed
{
    public class Context : DataModels.bssContext
    {
        public Context()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
            IConfiguration configuration = configurationBuilder.Build();

            optionsBuilder.UseMySql(configuration.GetValue<string>("ConnectionString:DefaultConnection"));
        }
    }
}