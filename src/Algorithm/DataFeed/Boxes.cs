﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Algorithm.DataFeed
{
    public class Boxes : Base
    {
        public Boxes() : base()
        {
        }

        public List<Algorithm.Models.Logisticbox> Get()
        {
            return context.Logisticbox.Join(
                context.Box,
                lb => lb.BoxId,
                b => b.BoxId,
                (lb, b) => new
                {
                    LogisticId = lb.LogisticId,
                    LogisticboxId = lb.LogisticboxId,
                    BoxId = lb.BoxId,
                    BoxName = b.Name,
                    Price = lb.Price,
                    Weight = lb.Weight,
                    Length = b.Length,
                    Width = b.Width,
                    Height = b.Height
                }).Select(p => new Algorithm.Models.Logisticbox
                {
                    LogisticboxId = p.LogisticboxId,
                    LogisticId = p.LogisticId,
                    BoxId = p.BoxId,
                    BoxName = p.BoxName,
                    Price = p.Price,
                    Weight = Convert.ToInt32(p.Weight * 1000),
                    Length = Convert.ToInt32(p.Length * 10),
                    Width = Convert.ToInt32(p.Width * 10),
                    Height = Convert.ToInt32(p.Height * 10)
                }).ToList();
        }

        public List<int> GetLogisticIds()
        {
            return context.Logisticinfo.Select(l => l.LogisticId).ToList();
        }

        public List<Algorithm.Models.Logisticbox> GetByLogisticId(int logisticId)
        {
            return context.Logisticbox.Join(
                context.Box,
                lb => lb.BoxId,
                b => b.BoxId,
                (lb, b) => new
                {
                    LogisticId = lb.LogisticId,
                    LogisticboxId = lb.LogisticboxId,
                    BoxId = lb.BoxId,
                    BoxName = b.Name,
                    Price = lb.Price,
                    Weight = lb.Weight,
                    Length = b.Length,
                    Width = b.Width,
                    Height = b.Height
                }).Where(
                    res => res.LogisticId == logisticId
                ).Select(res => new Algorithm.Models.Logisticbox
                {
                    LogisticboxId = res.LogisticboxId,
                    LogisticId = res.LogisticId,
                    BoxId = res.BoxId,
                    BoxName = res.BoxName,
                    Price = res.Price,
                    Weight = Convert.ToInt32(res.Weight * 1000),
                    Length = Convert.ToInt32(res.Length * 10),
                    Width = Convert.ToInt32(res.Width * 10),
                    Height = Convert.ToInt32(res.Height * 10)
                }).ToList();
        }
    }
}