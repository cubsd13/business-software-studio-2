﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SBA.Algorithm.DataFeed
{
    public class Order : Base
    {
        private int _id;

        public Order(int OrderId) : base()
        {
            this._id = OrderId;
        }

        public List<Algorithm.Models.Order> GetItems()
        {
            return context.Ordersdetail.Join(
                context.Product,
                od => od.ProductId,
                p => p.ProductId,
                (od, p) => new
                {
                    OrderId = od.OrdersId,
                    ProductId = od.ProductId,
                    Qty = od.Qty,
                    Name = p.Name,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    Weight = p.Weight
                }).Where(
                    res => res.OrderId == this._id
                ).Select(res => new Algorithm.Models.Order
                {
                    OrderId = res.OrderId,
                    ProductId = res.ProductId,
                    Qty = res.Qty,
                    Name = res.Name,
                    Length = Convert.ToInt32(res.Length * 10),
                    Width = Convert.ToInt32(res.Width * 10),
                    Height = Convert.ToInt32(res.Height * 10),
                    Weight = Convert.ToInt32(res.Weight),
                }).OrderByDescending(res => res.Weight).ThenByDescending(res => res.Length).ToList();
        }
    }
}