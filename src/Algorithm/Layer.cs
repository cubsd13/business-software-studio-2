using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace SBA.Algorithm
{
    public class Layer
    {
        private int length; // X
        private int width;  // Y
        private int height; // Z
        private int weight;

        public List<Block> blocks = new List<Block>();

        public List<Block> Blocks
        {
            get { return blocks; }
            set { blocks = value; }
        }

        public int Length
        {
            get { return this.length; }
        }

        public int Width
        {
            get { return this.width; }
        }

        public int Height
        {
            get { return this.height; }
        }

        public int Weight
        {
            get { return this.weight; }
        }

        public int VolumeOfBlock
        {
            get { return this.blocks.Sum(block => block.Volume); }
        }

        public int CurrentHeight
        {
            get { return (this.blocks.Count <= 0) ? 0 : this.blocks.Max(block => block.Height); }
        }

        public int CurrentHeightMedian
        {
            get
            {
                if (this.blocks.Count <= 0) return 0;

                List<Block> b = this.blocks.OrderBy(b => b.Height).ToList();
                return b[b.Count / 2].Height;
            }
        }

        public int CurrentWeight
        {
            get { return this.blocks.Sum(block => block.Weight); }
        }

        public bool isEmpty
        {
            get { return (!this.blocks.Any()) ? true : false; }
        }

        public bool isWeightValid
        {
            get { return (this.CurrentWeight > this.weight) ? false : true; }
        }

        public int totalBlock
        {
            get { return this.blocks.Where(b => b.ProductId > 0).Count(); }
        }

        public Layer(int length, int width, int height, int weight)
        {
            this.length = length;
            this.width = width;
            this.height = height;
            this.weight = weight;
        }

        public Block GetBlock(int cx, int cy)
        {
            Block block = this.Blocks.Find(b => (b.X < cx && b.X + b.Length > cx) && (b.Y < cy && b.Y + b.Width > cy));
            return (block is null) ? new Block() : block;
        }

        public bool AddBlock(Block block)
        {
            if (!this.isPlaceableByWeight(block.Weight))
            {
                return false;
            }

            for (int x = 0; x < this.Length; x++)
            {
                for (int y = 0; y < this.Width; y++)
                {
                    Program.cycleCount++;
                    Block tb = this.GetBlock(x, y);
                    if ((tb.Y + tb.Width) > 0) y = tb.Y + tb.Width;

                    if (this.isPlaceable(x, y, block.Length, block.Width))
                    {
                        block.X = x;
                        block.Y = y;
                        this.Blocks.Add(block);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool isPlaceable(int x, int y, int length, int width)
        {
            foreach (Block block in this.blocks)
            {
                if ((new Rectangle(x, y, length, width)).IntersectsWith(
                    new Rectangle(block.X, block.Y, block.Length, block.Width)))
                {
                    return false;
                }
            }

            if ((x + length) > this.Length) return false;
            if ((y + width) > this.Width) return false;

            return true;
        }

        private bool isPlaceableByWeight(int weight)
        {
            if ((this.CurrentWeight + weight) > this.Weight)
            {
                Console.WriteLine("Layer:isPlaceableByWeight {0}/{1} => {2}/{1}", this.CurrentWeight, this.weight, this.CurrentWeight + weight);
                return false;
            }

            return true;
        }

        public List<Block> AddBlock(List<Block> blocks)
        {
            foreach (Block block in blocks)
            {
                if (this.AddBlock(block))
                {
                    blocks.Remove(block);
                }
            }

            return blocks;
        }

        public Color Background { get; set; } = Color.White;

        public string Export()
        {
            Bitmap bitmap = new Bitmap(this.Length + 1, this.Width + 1);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.Clear(this.Background);

            foreach (Block block in blocks)
            {
                Rectangle rectangle = new Rectangle(
                    block.X,
                    block.Y,
                    block.Length,
                    block.Width
                );

                Brush brush = new SolidBrush(block.Color);

                graphics.FillRectangle(brush, rectangle);
                graphics.DrawRectangle(Pens.Black, rectangle);

                graphics.DrawString(
                    block.ProductId.ToString(),
                    new Font("Arial", 16),
                    new SolidBrush(Color.Black),
                    block.X + (block.Length / 2) - (block.ProductId.ToString().Count() * 8),
                    block.Y + (block.Width / 2) - 8,
                    new StringFormat()
                );
            }

            MemoryStream memory = new MemoryStream();

            bitmap.Save(
                memory,
                ImageFormat.Png
            );

            return Convert.ToBase64String(memory.ToArray());
        }
    }
}